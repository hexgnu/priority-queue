include ./Makefile.inc
simple_project:
	$(PREFIX)gnatmake -Psimple.gpr
ifeq ($(RUN_DINERS),true)
	$(CDir)main$(EXE)
endif

clean:
	$(PREFIX)gnatclean -Psimple.gpr

.PHONY: clean simple_project
