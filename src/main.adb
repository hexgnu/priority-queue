with Priority_Queue;
with Ada, Ada.Text_IO;
use Ada;

procedure Main is

  type Title_Type is (Staff, Manager, Vice_President, President);

  type Work_Request_Record is
     record
        Name, Description: String (1..5);
        Title:  Title_Type;
     end record;

  function Title_Of (Request: Work_Request_Record) return Title_Type is
  begin
     return Request.Title;
  end Title_Of;

  package Job_Queue is new Priority_Queue (
     Element_Type=>Work_Request_Record,
     Priority_Type=>Title_Type,
     Priority_Of=>Title_Of);

  Queue: Job_Queue.Queue_Type (100);
  Item: Work_Request_Record;

begin
  Job_Queue.Enqueue (Queue, ("AG   ", "more ", Vice_President));
  Job_Queue.Enqueue (Queue, ("WJC  ", "stuff", President));
  Job_Queue.Dequeue (Queue, Item);
  Text_IO.Put_Line (Item.Name);
end Main;